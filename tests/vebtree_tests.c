#include <stdlib.h>
#include <time.h>
#include <Trees/minunit.h>
#include <Trees/vebtree.h>

#define TEST_SIZE 1024

static struct vebtree *yggdrasil = NULL;
static vebtree_value sorted_test_array[TEST_SIZE] = {0};


void setup_test_array()
{
  int index;
  for(index = 0; index < TEST_SIZE; index++)
    sorted_test_array[index] = index;
}

/* tests also low and high, since if these work, then by extension */
/* x = index(high(x),low(x)), and its easier to test all at once   */
char *test_vebtree_index()
{
  int test_value = TEST_SIZE-1;
  int index = vebtree_index(TEST_SIZE,vebtree_high(TEST_SIZE,test_value),vebtree_low(TEST_SIZE,test_value));
  mu_assert(index == test_value, "It should hold that x = index(high(x),low(x)");

  return NULL;
}

char *test_create()
{
  yggdrasil = vebtree_create(TEST_SIZE);
  mu_assert(yggdrasil != NULL, "Failed to create vebtree");

  return NULL;
}

char *test_insert()
{
  int index;
  for(index = 1; index < TEST_SIZE; index++)
    {
      mu_assert(vebtree_member(yggdrasil, index) == 0, "Member should return false before insert.");

      int rc = vebtree_insert(yggdrasil, index);
      mu_assert(rc != VEBTREE_ERROR, "Failed to insert.");
      mu_assert(vebtree_member(yggdrasil, index) == 1, "Member should return true for newly inserted element.");
    }
  return NULL;
}
char *test_delete()
{
	int index;
	for(index = 1; index < TEST_SIZE-1; index++)
	{
		int member_before = vebtree_member(yggdrasil,index);
		mu_assert(member_before == 1, "Member should return true before element deletion");

		int rc = vebtree_delete(yggdrasil, index);
		mu_assert(rc != VEBTREE_ERROR, "Failed to delete.");
		mu_assert(vebtree_member(yggdrasil, index) == 0, "Member should return false for newly deleted element.");
		mu_assert(vebtree_find_min(yggdrasil) == index+1, "New minimum is not correct!");
	}

  return NULL;
}

char *test_delete_min()
{
	/* kør find min, gem res */
	int index;
	for(index = 1; index < TEST_SIZE-1; index++)
	{
		int findmin_before = vebtree_find_min(yggdrasil);
		int rc = vebtree_delete_min(yggdrasil);
		mu_assert(rc != VEBTREE_ERROR, "Failed to delete min.");
		mu_assert(vebtree_member(yggdrasil,findmin_before) == 0, "find min before should now be deleted");
	}
	/* kør delete min */

	/* tjek om gem res er member (burde være slettet!) */


	return NULL;
}

char *test_destroy()
{
  vebtree_destroy(yggdrasil);
  return NULL;
}

char *test_find_succ()
{
  yggdrasil = vebtree_create(TEST_SIZE);
  mu_assert(yggdrasil != NULL, "Failed to create vebtree");
  int index;
  for(index = 2; index < TEST_SIZE; index = index+2)
    {
      //printf("Trying to insert %d\n", index);
      int rc = vebtree_insert(yggdrasil, index);
      mu_assert(rc != VEBTREE_ERROR, "Failed to insert.");
    }

  for(index = 1; index < TEST_SIZE-1; index++)
    {
      int succ = vebtree_find_succ(yggdrasil, index);
      //printf("The successor of %d was found to be %d and should be %d\n", index, succ, 2*((index+1)/2));
      mu_assert(succ == 2*((index+1)/2), "Returned wrong successor.");
    }
  vebtree_destroy(yggdrasil);
  yggdrasil = NULL;
  return NULL;
}

char *test_pred_search()
{
  struct vebtree *yggdrasil_temp = vebtree_create(TEST_SIZE);
  mu_assert(yggdrasil_temp != NULL, "Failed to create vebtree.");
  int index;
  for(index = 2; index < TEST_SIZE; index = index + 2)
  {
    int rc = vebtree_insert(yggdrasil_temp, index);
    mu_assert(rc == VEBTREE_OK, "Failed to insert.");
  }

  for(index = 3; index < TEST_SIZE-1; index++)
  {
    int pred = vebtree_predecessor_search(yggdrasil_temp, index);
    int expected = 2*(index/2);
   
    mu_assert(pred == expected,"Returned wrong predecessor.");
  }

  vebtree_destroy(yggdrasil_temp);
  return NULL;
}

char *all_tests()
{
  srand(time(NULL));
  setup_test_array();

  mu_suite_start();

  mu_run_test(test_find_succ);
  mu_run_test(test_create);
  mu_run_test(test_vebtree_index);
  mu_run_test(test_insert);
  mu_run_test(test_delete);
  mu_run_test(test_destroy);

  /* do it all again, but now we run delete_min */
  mu_run_test(test_create);
  mu_run_test(test_insert);
  mu_run_test(test_delete_min);
  mu_run_test(test_pred_search);
  mu_run_test(test_destroy);


  return NULL;
}

RUN_TESTS(all_tests);



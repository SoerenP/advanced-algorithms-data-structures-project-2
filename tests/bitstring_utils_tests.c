#include <stdlib.h>

#include <Trees/minunit.h>
#include <Trees/bitstring_utils.h>

#define TEST_SIZE 64
#define TRUE 1
#define FALSE 0

static struct bitstring b;

char *test_max_int_from_bitstring()
{
  struct bitstring temp = bitstring_create(32);
  int index;
  for(index = 1; index < 32 + 1; index++){
    bitstring_insert_bit(&temp,index);
  }

  for(index = 1; index < 32 + 1; index++){
    mu_assert(bitstring_test_bit(&temp, index) == 1, "All bits should be set");
  }

  return NULL;
}

char *test_create()
{
  b = bitstring_create(TEST_SIZE);
  return NULL;
}

char *test_member_empty()
{
  int index;
  for(index = 0; index < TEST_SIZE; index++)
  {
    mu_assert(bitstring_test_bit(&b,index) == 0, "Should be empty after create.");
  }
  return NULL;
}

char *test_insert()
{
  int test = bitstring_test_bit(&b,2);
  mu_assert(test == FALSE, "Bit should not be set.");

  bitstring_insert_bit(&b,2);
  test = bitstring_test_bit(&b,2);
  mu_assert(test == TRUE, "Bit should be set");

  bitstring_insert_bit(&b,33);
  test = bitstring_test_bit(&b,33);
  mu_assert(test == TRUE, "Bit should be set");
  return NULL;
}

char *test_msb()
{
  int highest = bitstring_msb_naive(&b);
  mu_assert(highest == 33, "Returned wrong msb"); 

  bitstring_delete_bit(&b,33);

  highest = bitstring_msb_naive(&b);
  mu_assert(highest == 2, "Returned wrong msb");

  return NULL;
}

char *test_lsb()
{
  int lowest = bitstring_lsb_naive(&b);
  mu_assert(lowest == 2, "Returned wrong lsb");

  bitstring_delete_bit(&b,2);
  bitstring_insert_bit(&b,34);
  lowest = bitstring_lsb_naive(&b);
  mu_assert(lowest == 34, "Returned wrong lsb");

  return NULL;
}

char *test_delete()
{
  int test = bitstring_test_bit(&b,45);
  mu_assert(test == FALSE, "bit should not be set");

  bitstring_insert_bit(&b,45);
  test = bitstring_test_bit(&b,45);
  mu_assert(test == TRUE, "bit should be set");

  bitstring_delete_bit(&b,45);
  test = bitstring_test_bit(&b,45);
  mu_assert(test == FALSE, "bit should be deleted");

  return NULL;
}

char *all_tests()
{
  mu_suite_start();
  mu_run_test(test_create);
  mu_run_test(test_member_empty);
  mu_run_test(test_insert);
  mu_run_test(test_msb);
  mu_run_test(test_lsb);
  mu_run_test(test_delete);

  mu_run_test(test_max_int_from_bitstring);
  return NULL;
}

RUN_TESTS(all_tests);

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <Trees/minunit.h>
#include <Trees/rbtree.h>

#define INIT_VAL 100
#define TEST_SIZE 10000
#define RANGE 1000
static struct rbtree *tree = NULL;
static struct node *node_array[TEST_SIZE];

char *test_create()
{
	tree = rbtree_create(INIT_VAL);
	mu_assert(tree != NULL, "Failed to create rbtree.");

	return NULL;
}

char *test_insert()
{
	int index = 0;
	for(index = 0; index < TEST_SIZE; index++)
	{
		int random_value = index+1;
		node_array[index] = rbtree_insert(tree,random_value);

		int member = rbtree_incl_predecessor(tree,random_value);
		mu_assert(member == random_value, "Should be inserted.");
	}

	return NULL;
}

char *test_free()
{
	rbtree_destroy(tree);

	return NULL;
}

char *test_delete()
{
	struct rbtree *test_tree = rbtree_create(1);
	struct node *nodes[TEST_SIZE];
	nodes[0] = test_tree->root;

	int index = 0;
	
	for(index = 1; index < TEST_SIZE; index++)
	{
		nodes[index] = rbtree_insert(test_tree,index+1);
	}

	for(index = 1; index < TEST_SIZE-1; index++)
	{
		struct node *to_delete = nodes[index];
		int to_delete_value = to_delete->val;
		rbtree_delete(test_tree,to_delete);
		free(to_delete);
		struct node *found_min = rbtree_min(test_tree,test_tree->root);
		mu_assert(found_min->val != to_delete_value, "Should not be able to be value of pred. query after delete.");
	}

	rbtree_destroy(test_tree);

	return NULL;
}


char *test_delete_min()
{
	struct rbtree *test_tree = rbtree_create(1);
	struct node *nodes[TEST_SIZE];
	nodes[0] = test_tree->root;

	int index = 0;
	
	for(index = 1; index < TEST_SIZE; index++)
	{
		nodes[index] = rbtree_insert(test_tree,index+1);
	}

	for(index = 1; index < TEST_SIZE-1; index++)
	{
		rbtree_delete_min(test_tree);
		struct node *found_min = rbtree_min(test_tree,nodes[0]);
		mu_assert(found_min->val != index, "Should not be able to be value of pred. query after delete.");
	}

	rbtree_destroy(test_tree);

	return NULL;
}


char *test_insertion_deletion_random()
{
	struct rbtree *test_tree = rbtree_create(1);
	struct node *nodes[TEST_SIZE];
	
	int index;
	for(index = 0; index < TEST_SIZE; index++)
	{
		nodes[index] = rbtree_insert(test_tree,rand());
	
		int correctness = rbtree_check_conditions(test_tree,test_tree->root);
		if (correctness != 0) printf("Index %d when the thingy fails with correctness %d\n", index, correctness);
		mu_assert(correctness == 0, "Conditions not satisfied for insert");
	}

	for(index = 0; index < TEST_SIZE; index++)
	{	
		struct node *target = nodes[index];
		rbtree_delete(test_tree,target);
		free(target);
		int correctness = rbtree_check_conditions(test_tree,test_tree->root);
		mu_assert(correctness == 0, "Conditions not satisfied for delete.");
	}

	rbtree_destroy(test_tree);

	return NULL;
}

char *test_predecessor()
{
	struct rbtree *temp_tree = rbtree_create(1);

	int index;
	for(index = 2; index < TEST_SIZE; index = index + 2)
	{
		rbtree_insert(temp_tree,index);
	}

	for(index = 2; index < TEST_SIZE; index++)
	{
		int pred = rbtree_incl_predecessor(temp_tree,index);
		int expected = 2*(index/2);
		mu_assert(pred == expected, "Failed to retrieve correct predecessor.");
	}

	rbtree_destroy(temp_tree);

	return NULL;
}

char *test_iterative_search()
{
	struct rbtree *temp_tree = rbtree_create(1);

	int index;
	for(index = 2; index < TEST_SIZE; index++)
	{
		struct node *before = rbtree_iter_search(temp_tree,index);
		mu_assert(before->val != index, "Should not be inserted before insertion.");

		rbtree_insert(temp_tree,index);

		struct node *after = rbtree_iter_search(temp_tree,index);
		mu_assert(after->val == index, "Should be inserted after insertion.");
	}

	rbtree_destroy(temp_tree);

	return NULL;
}

char *all_tests()
{

	srand(time(NULL));
	mu_suite_start();

	mu_run_test(test_create);
	mu_run_test(test_insert);
	mu_run_test(test_delete);
	mu_run_test(test_free);

	mu_run_test(test_predecessor);
	mu_run_test(test_iterative_search);
	mu_run_test(test_insertion_deletion_random);
	mu_run_test(test_delete_min);

	return NULL;
}

RUN_TESTS(all_tests);

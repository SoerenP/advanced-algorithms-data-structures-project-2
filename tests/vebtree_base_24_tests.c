#include <stdlib.h>
#include <time.h>
#include <Trees/minunit.h>
#include <Trees/vebtree_base_24.h>
#include <Trees/math_util.h>
#include <Trees/bool.h>

#define TEST_SIZE 4096

static struct vebtree_base_24 *yggdrasil = NULL;

char *test_upper_square()
{
  int res = upper_square(VEBTREE_BASE_24_DEFAULT_SIZE);
  int expected = 4096;
  mu_assert(res == expected, "Wrong result for upper square");

  return NULL;
}

char *test_create()
{
  yggdrasil = vebtree_base_24_create(upper_square(VEBTREE_BASE_24_DEFAULT_SIZE));
  mu_assert(yggdrasil != NULL, "Failed to create vebtree");

  return NULL;
}

char *test_insert()
{
  int index;
  int min = 1;
  for(index = 1; index < TEST_SIZE; index++)
  {
    int member = vebtree_base_24_member(yggdrasil,index);
    mu_assert(member == 0, "Should not be member before insert.");

    int rc = vebtree_base_24_insert(yggdrasil, index);
    mu_assert(rc == VEBTREE_BASE_24_OK, "Failed to insert.");

    int found_min = vebtree_base_24_find_min(yggdrasil);
    mu_assert(found_min == min, "Should not change min when inserting in sorted order.");

    member = vebtree_base_24_member(yggdrasil,index);
    mu_assert(member != 0, "Should be member after insertion");
  }

  return NULL;
}

char *test_delete()
{
  int index;
  for(index = 1; index < TEST_SIZE-1; index++)
  {
    int member = vebtree_base_24_member(yggdrasil,index);
    mu_assert(member == 1, "Should be member before delete since we inserted");

    int rc = vebtree_base_24_delete(yggdrasil,index);
    mu_assert(rc == VEBTREE_BASE_24_OK, "Failed to delete");

    int new_min = vebtree_base_24_find_min(yggdrasil);
    mu_assert(new_min == index+1, "New minimum incorrect after delete");


    member = vebtree_base_24_member(yggdrasil, index);
    mu_assert(member == 0, "Should not be member after delete.");
  }

  return NULL;
}

char *test_delete_min()
{
  struct vebtree_base_24 *yggdrasil_temp = vebtree_base_24_create(TEST_SIZE);
  mu_assert(yggdrasil_temp != NULL, "Failed to create vebtree.");
  int index;
  for(index = 1; index < TEST_SIZE; index++)
  {
    int rc = vebtree_base_24_insert(yggdrasil_temp, index);
    mu_assert(rc != VEBTREE_BASE_24_ERROR, "Failed to insert");
  }

  for(index = 1; index < TEST_SIZE; index++)
  {
    int found_min = vebtree_base_24_find_min(yggdrasil_temp);
    int member_before = vebtree_base_24_member(yggdrasil_temp,found_min);
    mu_assert(member_before == TRUE, "Should be member before deletion");

    vebtree_base_24_value temp = vebtree_base_24_delete_min(yggdrasil_temp);
    mu_assert(temp == VEBTREE_BASE_24_OK, "Failed to delete min.");

    int member_after = vebtree_base_24_member(yggdrasil_temp,found_min);
    mu_assert(member_after == FALSE, "Should not be member after delete.");
  }
  
  vebtree_base_24_destroy(yggdrasil_temp);
  return NULL;
}

char *test_find_succ()
{
  struct vebtree_base_24 *yggdrasil_temp = vebtree_base_24_create(TEST_SIZE);
  mu_assert(yggdrasil_temp != NULL, "Failed to create vebtree");
  int index;
  for(index = 2; index < TEST_SIZE; index = index+2)
    {
      int rc = vebtree_base_24_insert(yggdrasil_temp, index);
      mu_assert(rc != VEBTREE_BASE_24_ERROR, "Failed to insert.");
    }

  for(index = 1; index < TEST_SIZE-1; index++)
    {
      int succ = vebtree_base_24_find_succ(yggdrasil_temp, index);
      mu_assert(succ == 2*((index+1)/2), "Returned wrong successor.");
    }
  vebtree_base_24_destroy(yggdrasil_temp);
  yggdrasil_temp = NULL;
  return NULL;
}

char *test_pred_search()
{
  struct vebtree_base_24 *yggdrasil_temp = vebtree_base_24_create(TEST_SIZE);
  mu_assert(yggdrasil_temp != NULL, "Failed to create vebtree.");
  int index;
  for(index = 2; index < TEST_SIZE; index = index + 2)
  {
    int rc = vebtree_base_24_insert(yggdrasil_temp, index);
    mu_assert(rc == VEBTREE_BASE_24_OK, "Failed to insert.");
  }

  for(index = 3; index < TEST_SIZE-1; index++)
  {
    int pred = vebtree_base_24_predecessor_search(yggdrasil_temp, index);
    int expected = 2*(index/2);
    mu_assert(pred == expected,"Returned wrong predecessor.");
  }

  vebtree_base_24_destroy(yggdrasil_temp);
  return NULL;
}

char *test_destroy()
{
  vebtree_base_24_destroy(yggdrasil);
  return NULL;
}

char *all_tests()
{
  srand(time(NULL));

  mu_suite_start();

  mu_run_test(test_upper_square);

  mu_run_test(test_create);
  mu_run_test(test_insert);
  mu_run_test(test_delete);
  mu_run_test(test_find_succ);
  mu_run_test(test_destroy);
  mu_run_test(test_delete_min);
  mu_run_test(test_pred_search);
  return NULL;
}

RUN_TESTS(all_tests);

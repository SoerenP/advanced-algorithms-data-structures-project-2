###### Contributors: Søren Elmely Pettersson & Simon Enni(where)


The Makefile builds a statically linked library called libTrees.a, which containts all sourcecode. 

Running "make clean all" builds the static library, runs all tests automatically, and compiles all experiments in the bin/ folder.

Since linking with gcc is platform dependant, so is the makefile. This is because on some platforms, the library name should be AFTER the linking argument -L, on other platforms it should be BEFORE the linking argument to the compiler. If the compilation fails due to linking, try manipulation the Makefile as to link the static library in an order that is appropriate to your specific platform.

Platform that the compilation succeeds on are known to be:

Linux Ubuntu 14.04
Linux Fedora

All code, when used as portrayed in the tests and experiments, is, to the best of our knowledge, free from memory leakage and memory corruption. All implementations where run under valgrind to assure this. 

#usage: gather_data.sh heap datafile iterations 
#heap {fheap,heap}
#!/bin/bash
k=$3

echo "Running experiment"
./gather_time_$1.sh $k | tee ../data/$2

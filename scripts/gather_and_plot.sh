#usage: gather_and_plot.sh heap operation datafile iterations 
#heap {fheap,heap}
#operation {make, insert, findmin, delete, decrease}
#!/bin/bash
k=$4

echo "Running experiment"
gather_time_$1.sh $k | tee ../data/$3

echo "Plotting with gnuplot"
if [ $2 = make ]
	then gnuplot -c plot_makeheap.gp ../data/$3 $1$3
elif [ $2 = insert ]
	then gnuplot -c plot_insert.gp ../data/$3 $1$3
elif [ $2 = findmin ]
	then gnuplot -c plot_deletemin.gp ../data/$3 $1$3
elif [ $2 = delete ]
	then gnuplot -c plot_decreasekey.gp ../data/$3 $1$3
elif [ $2 = decrease ]
	then gnuplot -c plot_findmin.gp ../data/$3 $1$3
fi

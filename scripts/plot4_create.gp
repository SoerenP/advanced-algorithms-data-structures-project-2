# Usage:
#	gnuplot -c plot_create.gp file1 file2 file3 file4 plottitle ytitle #xtitle
set title ARG6
set ylabel ARG7
set xlabel ARG8
set logscale x 2
set format x '2^{%L}'
set key right center
set logscale y 2
plot 	ARG1 using 1:2 with linespoints title "naive", \
			ARG2 using 1:2 with linespoints title "base24", \
			ARG3 using 1:2 with linespoints title "heap",\
			ARG4 using 1:2 with linespoints title "fheap", \
			ARG5 using 1:2 with linespoints title "naive varying universe"
pause -1 "Hit any key to continue"


# Usage:
#	gnuplot -c plot_create.gp file1 file2 file3 plottitle ytitle xtitle
# file1label file2label file3label
set title ARG4
set ylabel ARG5
set xlabel ARG6
set logscale x 2
set format x '2^{%L}'
set key right center
set logscale y 2
plot 	ARG1 using 1:4 with linespoints title ARG7, \
			ARG2 using 1:4 with linespoints title ARG8, \
			ARG3 using 1:4 with linespoints title ARG9
pause -1 "Hit any key to continue"


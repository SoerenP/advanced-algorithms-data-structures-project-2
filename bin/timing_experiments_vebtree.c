#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <Trees/vebtree.h>

#define EXPECTED_ARGS 3
#define ITERATIONS 1000
#define UNIVERSE_SIZE 16777216

double time_make_tree()
{
	clock_t begin,end;
	double time_spent = 0.0;

	/* begin counting */
	begin = clock();

	/* do work */
	struct vebtree *T = vebtree_create(UNIVERSE_SIZE);

	/* stop counting */
	end = clock();

	/* cleanup */
	vebtree_destroy(T);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;
}

double time_insert_tree(vebtree_value *A)
{
	clock_t begin,end;
	double time_spent = 0.0;

	struct vebtree *T = vebtree_create(UNIVERSE_SIZE);

	int index = 0;
	
	for(index = 0; index < ITERATIONS; index++)
	{
	  /* begin counting */
	  begin = clock();
	  /* do work */
	  vebtree_insert(T,A[index]);
	  /* stop counting */
	  end = clock();
	  vebtree_delete_min(T);
	  time_spent += (double)(end - begin)/CLOCKS_PER_SEC;
	}

        /* cleanup */
	vebtree_destroy(T);

	return time_spent;
}

double time_find_min_tree(vebtree_value *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	struct vebtree *T = vebtree_create(UNIVERSE_SIZE);


	
	int index = 0;

	for(index = 0; index < n; index++)
	{
	  vebtree_insert(T,A[index]);
	}
		
	vebtree_value temp;	

	/* begin counting */
	begin = clock();

	/* do work */
	for(index = 0; index < ITERATIONS; index++)
	{
		temp = vebtree_find_min(T);
	}

	/* stop counting */
	end = clock();
	temp++;

        /* cleanup */
	vebtree_destroy(T);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;
}

double time_delete_min_tree(vebtree_value *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	struct vebtree *T = vebtree_create(UNIVERSE_SIZE);

	int index = 0;
	for(index = 0; index < n; index++)
	{
	  vebtree_insert(T,A[index]);
	}
	vebtree_value temp;	

        for(index = 0; index < ITERATIONS; index++)
	{
	  /* begin counting */
	  begin = clock();
	  /* do work */
	  temp = vebtree_delete_min(T);
	  /* stop counting */
	  end = clock();
	  //vebtree_insert(T,A[index]);
	  time_spent += (double)(end - begin)/CLOCKS_PER_SEC;
	}
	temp++;

	/* cleanup */
	vebtree_destroy(T);

	return time_spent;
}

double time_delete_tree(vebtree_value *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	struct vebtree *T = vebtree_create(UNIVERSE_SIZE);

	int index = 0;
	for(index = 0; index < n; index++)
	{
	  vebtree_insert(T,A[index]);
	}
	vebtree_value temp;	

        for(index = 0; index < ITERATIONS; index++)
	{
	  /* begin counting */
	  begin = clock();
	  /* do work */
	  temp = vebtree_delete(T,A[index]);
	  /* stop counting */
	  end = clock();
	  vebtree_insert(T,A[index]);
	  time_spent += (double)(end - begin)/CLOCKS_PER_SEC;
	}
	temp++;

	/* cleanup */
	vebtree_destroy(T);

	return time_spent;
}

double time_succ_tree(vebtree_value *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	struct vebtree *T = vebtree_create(UNIVERSE_SIZE);

	int index = 0;
	for(index = 0; index < n; index++)
	{
	  vebtree_insert(T,A[index]);
	}
	vebtree_value temp;	

        for(index = 0; index < ITERATIONS; index++)
	{
	  /* begin counting */
	  begin = clock();
	  /* do work */
	  temp = vebtree_find_succ(T,index);
	  /* stop counting */
	  end = clock();
	  //vebtree_insert(T,A[index]);
	  time_spent += (double)(end - begin)/CLOCKS_PER_SEC;
	}
	temp++;

	/* cleanup */
	vebtree_destroy(T);

	return time_spent;
}


double time_pred_tree(vebtree_value *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	struct vebtree *T = vebtree_create(UNIVERSE_SIZE);

	int index = 0;
	for(index = 0; index < n; index++)
	{
	  vebtree_insert(T,A[index]);
	}
	vebtree_value temp;	

        for(index = 1; index < ITERATIONS+1; index++)
	{
	  /* begin counting */
	  begin = clock();
	  /* do work */
	  temp = vebtree_predecessor_search(T,index);
	  /* stop counting */
	  end = clock();
	  //vebtree_insert(T,A[index]);
	  time_spent += (double)(end - begin)/CLOCKS_PER_SEC;
	}
	temp++;

	/* cleanup */
	vebtree_destroy(T);

	return time_spent;
}

void setup_test_array(vebtree_value *V, int n)
{
  // Shuffling algorithm
  if (n > 1) 
    {
      int i;
      for (i=0; i < n; i++)
	{
	  V[i] = i * (UNIVERSE_SIZE/n);
	}
      
      for (i = 0; i < n - 1; i++) 
        {
          size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
          int t = V[j];
          V[j] = V[i];
          V[i] = t;
        }
    }
}

float average(float *A, int size)
{
	int index = 0;
	float sum = 0;
	for(index = 0; index < size; index++)
	{
		sum += A[index];
	}

	return sum / size;
}

int main(int argc, char **argv)
{
	if(argc < EXPECTED_ARGS){
		printf("Usage: %s n (elements) k (iterations)\n",argv[0]);
		exit(1);
	}

	/* setup stuff */
	srand(time(NULL));

	int n = atoi(argv[1]);
	int k = atoi(argv[2]);

	float time_insert[k];
	float time_deletemin[k];
	float time_tree_create[k];
	float time_tree_find_min[k];
	float time_tree_delete[k];
	float time_tree_succ[k];
	float time_tree_pred[k];

	printf("#----Timing results for tree at n = %d, k = %d----\n",n,k);
	printf("#n \tmaketree \tinsert \tdeleteMin \tdelete \tFindMin \tSucc \tPred\n");

	int iterations = 0;
	for(iterations = 0; iterations < k; iterations++)
	  {
	    int array_size = n < ITERATIONS ? ITERATIONS : n;			
	    vebtree_value *test_array = calloc(sizeof(vebtree_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_tree_create[iterations] = time_make_tree();
	    free(test_array);

	    test_array = calloc(sizeof(vebtree_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_insert[iterations] = time_insert_tree(test_array);
	    free(test_array);

	    test_array = calloc(sizeof(vebtree_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_tree_find_min[iterations] = time_find_min_tree(test_array, n);
	    free(test_array);

	    test_array = calloc(sizeof(vebtree_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_deletemin[iterations] = time_delete_min_tree(test_array,n);
	    free(test_array);

	    test_array = calloc(sizeof(vebtree_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_tree_delete[iterations] = time_delete_tree(test_array,n);
	    free(test_array);

	    test_array = calloc(sizeof(vebtree_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_tree_succ[iterations] = time_succ_tree(test_array,n);
	    free(test_array);

	    test_array = calloc(sizeof(vebtree_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_tree_pred[iterations] = time_pred_tree(test_array,n);
	    free(test_array);
	  }

	float avrg_insert_time = average(time_insert,k);
	float avrg_delete_min = average(time_deletemin,k);
	float avrg_tree_create = average(time_tree_create,k);
	float avrg_tree_find_min = average(time_tree_find_min,k);
	float avrg_delete = average(time_tree_delete,k);
	float avrg_succ = average(time_tree_succ,k);
	float avrg_pred = average(time_tree_pred,k);
	
	printf("%d \t%f \t%f \t%f \t%f \t%f \t%f \t%f\n",n,avrg_tree_create,avrg_insert_time,avrg_delete_min,avrg_delete,avrg_tree_find_min, avrg_succ, avrg_pred);

	return 0;
}

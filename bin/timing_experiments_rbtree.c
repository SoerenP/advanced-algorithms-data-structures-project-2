#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <Trees/rbtree.h>

#define EXPECTED_ARGS 3
#define ITERATIONS 1000

double time_make_rbtree(int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	/* begin counting */
	begin = clock();

	/* do work */
	struct rbtree *T = rbtree_create(n);

	/* stop counting */
	end = clock();

	/* cleanup */
	rbtree_destroy(T);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;
}

double time_insert_tree(node_value *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	struct rbtree *T = rbtree_create(n);
	struct node **nodes = malloc(sizeof(struct node *) * ITERATIONS);
	int index = 0;

	for(index = 0; index < ITERATIONS; index++)
	{
		nodes[index] = rbtree_insert(T,A[index]);
	}

	for(index = ITERATIONS; index < n; index++)
	{
	  	rbtree_insert(T,A[index]);
	}

	/* repeatedly take some element and insert / delete it */

	for(index = 0; index < ITERATIONS; index++)
	{
	  rbtree_delete(T,nodes[index]);
	  /* begin counting */
	  begin = clock();
	  /* do work */
	  rbtree_insert(T,A[index]);
	  /* stop counting */
	  end = clock();
	  time_spent += (double)(end - begin)/CLOCKS_PER_SEC;
	  free(nodes[index]);
	}

	/* cleanup */
	rbtree_destroy(T);
	free(nodes);
	return time_spent;
}

double time_find_min_tree(node_value *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	struct rbtree *T = rbtree_create(n);
	
	int index = 0;

	for(index = 0; index < n; index++)
	{
	  rbtree_insert(T,A[index]);
	}
		
	/* begin counting */
	begin = clock();

	/* do work */
	for(index = 0; index < ITERATIONS; index++)
	{
		rbtree_min(T,T->root);
	}

	/* stop counting */
	end = clock();

        /* cleanup */
	rbtree_destroy(T);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;
}

double time_predecessor_tree(node_value *A, int n)
{
	clock_t begin, end;
	double time_spent = 0.0;

	struct rbtree *T = rbtree_create(n);

	int index;
	for(index = 0; index < n; index++)
	{
		rbtree_insert(T,A[index]);
	}

	begin = clock();
	for(index = 0; index < ITERATIONS; index++)
	{
		/* *A is permuted, so should be random when we just take index */
		rbtree_incl_predecessor(T,A[index]);
	}
	end = clock();

	rbtree_destroy(T);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;
}

double time_delete_min_tree(node_value *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	struct rbtree *T = rbtree_create(n);

	int index = 0;
	for(index = 0; index < n; index++)
	{
	  rbtree_insert(T,A[index]);
	}

        for(index = 0; index < ITERATIONS; index++)
	{
	  /* begin counting */
	  begin = clock();
	  /* do work */
	  rbtree_delete_min(T);
	  /* stop counting */
	  end = clock();
	  rbtree_insert(T,A[index]);
	  time_spent += (double)(end - begin)/CLOCKS_PER_SEC;
	}

	/* cleanup */
	rbtree_destroy(T);

	return time_spent;
}

double time_delete_tree(node_value *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	struct rbtree *T = rbtree_create(n);
	struct node **nodes = malloc(sizeof(struct node *) * ITERATIONS);
	int index = 0;

	/* only save pointers for the ITERATIONS we will delete */
	for(index = 0; index < ITERATIONS; index++)
	{
		nodes[index] = rbtree_insert(T,A[index]);
	}

	for(index = ITERATIONS; index < n; index++)
	{
	  	rbtree_insert(T,A[index]);
	}

	/* if iterations (1k) is larger than n, we run out of stuff */	
        for(index = 0; index < ITERATIONS; index++)
	{
	  /* begin counting */
	  begin = clock();
	  /* do work */
	  rbtree_delete(T,nodes[index]);
	  /* stop counting */
	  end = clock();
	  rbtree_insert(T,A[index]);
	  time_spent += (double)(end - begin)/CLOCKS_PER_SEC;
	}

	/* cleanup */
	rbtree_destroy(T);
	for(index = 0; index < ITERATIONS; index++)
	{
		free(nodes[index]);
	}
	free(nodes);

	return time_spent;
}

void setup_test_array(node_value *V, int n)
{
  // Shuffling algorithm
  if (n > 1) 
    {
      int i;
      for (i=0; i < n; i++)
	{
	  V[i] = i;
	}
      
      for (i = 0; i < n - 1; i++) 
        {
          size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
          int t = V[j];
          V[j] = V[i];
          V[i] = t;
        }
    }
}

float average(float *A, int size)
{
	int index = 0;
	float sum = 0;
	for(index = 0; index < size; index++)
	{
		sum += A[index];
	}

	return sum / size;
}

int main(int argc, char **argv)
{
	if(argc < EXPECTED_ARGS){
		printf("Usage: %s n (elements) k (iterations)\n",argv[0]);
		exit(1);
	}

	/* setup stuff */
	srand(time(NULL));

	int n = atoi(argv[1]);
	int k = atoi(argv[2]);

	if(n < ITERATIONS){
		printf("number of elements n set to %d, must be larger than ITERATIONS %d\n",n,ITERATIONS);
		exit(1);
	}

	float time_insert[k];
	float time_deletemin[k];
	float time_tree_create[k];
	float time_tree_find_min[k];
	float time_tree_delete[k];
	float time_tree_predecessor[k];

	printf("#----Timing results for tree at n = %d, k = %d----\n",n,k);
	printf("#n \tmaketree \tinsert \tdeleteMin \tdelete \tFindMin \tpredecessor\n");

	int iterations = 0;
	for(iterations = 0; iterations < k; iterations++)
	  {
	    int array_size = n < ITERATIONS ? ITERATIONS : n;			
	    node_value *test_array = calloc(sizeof(node_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_tree_create[iterations] = time_make_rbtree(n);
	    free(test_array);

	    test_array = calloc(sizeof(node_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_insert[iterations] = time_insert_tree(test_array, n);
	    free(test_array);

	    test_array = calloc(sizeof(node_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_tree_find_min[iterations] = time_find_min_tree(test_array, n);
	    free(test_array);

	    test_array = calloc(sizeof(node_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_deletemin[iterations] = time_delete_min_tree(test_array,n);
	    free(test_array);
	    test_array = calloc(sizeof(node_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_tree_delete[iterations] = time_delete_tree(test_array,n);
	    free(test_array);
	    test_array = calloc(sizeof(node_value),array_size);
	    setup_test_array(test_array,array_size);
	    time_tree_predecessor[iterations] = time_predecessor_tree(test_array, array_size);

	    free(test_array);
	  }

	float avrg_insert_time = average(time_insert,k);
	float avrg_delete_min = average(time_deletemin,k);
	float avrg_tree_create = average(time_tree_create,k);
	float avrg_tree_find_min = average(time_tree_find_min,k);
	float avrg_delete = average(time_tree_delete,k);
	float avrg_predecessor = average(time_tree_predecessor,k);	

	printf("%d \t%f \t%f \t%f \t%f \t%f \t%f\n",n,avrg_tree_create,avrg_insert_time,avrg_delete_min,avrg_delete,avrg_tree_find_min, avrg_predecessor);

	return 0;
}

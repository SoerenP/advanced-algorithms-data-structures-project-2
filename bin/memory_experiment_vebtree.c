#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <Trees/vebtree.h>
#include <Trees/rbtree.h>
#include <Trees/vebtree_base_24.h>

#define EXPECTED_ARGS 2
#define UNIVERSE_SIZE 16777216

int main(int argc, char **argv)
{
	if(argc < EXPECTED_ARGS){
		printf("Usage: 0 is vebtree, 1 is vebtree base 24 and 2 is red-black tree\n");
		exit(1);
	}

	/* setup stuff */
	srand(time(NULL));

	int a = atoi(argv[1]);

	if (a == 0){
	  struct vebtree *T = vebtree_create(UNIVERSE_SIZE);
	  vebtree_destroy(T);
	} else if (a == 1){
	  struct vebtree_base_24 *T = vebtree_base_24_create(UNIVERSE_SIZE);
	  vebtree_base_24_destroy(T);
	} else if (a == 2){
	  
	  struct rbtree *T = rbtree_create(UNIVERSE_SIZE);
	  int i;
	  for (i = 0; i < UNIVERSE_SIZE; i++){
	    rbtree_insert(T,i);
	  }
	  rbtree_destroy(T);
	}
	
	
	return 0;
}

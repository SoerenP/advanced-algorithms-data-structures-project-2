#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <limits.h>
#include <Trees/bitstring_utils.h>

#define INT_LENGTH 32

/* hardcoded to contain 64 bits in total */
struct bitstring bitstring_create(int bits)
{
	struct bitstring new;
	new.n = bits;
	int integers = (int)ceil((float)new.n / (float)INT_LENGTH);
	int index = 0;
	for(index = 0; index < integers; index++)
		new.bits[index] = 0;

	return new;
}

void bitstring_insert_bit(struct bitstring *bs, int x)
{
  x--;
  assert(x < bs->n);
  bs->bits[x/INT_LENGTH] |= (1 << (x%INT_LENGTH));
}

void bitstring_delete_bit(struct bitstring *bs, int x)
{
  x--;
  assert(x < bs->n);
  bs->bits[x/INT_LENGTH] &= ~(1 << (x%INT_LENGTH));
}

int bitstring_test_bit(struct bitstring *bs, int x)
{
  x--;
  assert(x < bs->n);
  return ((bs->bits[x/INT_LENGTH] & (1<<(x%INT_LENGTH)))!=0);
}

int bitstring_succ(struct bitstring *bs, int x)
{
  assert(x <= bs->n);
  while (bitstring_test_bit(bs,x) != 1 && x < bs->n)
    {
      x++;
    }
  return x;
}

int bitstring_pred(struct bitstring *bs, int x)
{
  assert(x <= bs->n);
  while (bitstring_test_bit(bs,x) != 1 && x >= 0)
    {
      x--;
    }
  return x;
}

int bitstring_msb_naive(struct bitstring *bs)
{
  int indexes = bs->n / INT_LENGTH;

  /* if the underlying int is 0, then no bits are set, so check next */
  while(bs->bits[indexes-1] == 0 && indexes > 0){
	indexes -= 1;
  }
  /* now find the msb in that specific int */
  int i = 1;
  int x = bs->bits[indexes-1];
  while(x >>= 1) {++i;}
  return (indexes-1) * INT_LENGTH + i;
}

int bitstring_lsb_naive(struct bitstring *bs)
{
  int indexes = 0;
  while(bs->bits[indexes] == 0 && indexes < BIT_ARRAY_SIZE)
    indexes++;

  /*now find the lsb of that specific int */
  int x = bs->bits[indexes];
  x = x & (-x);
  int i = 1;
  while(x >>= 1) {++i;}
  return indexes * INT_LENGTH + i;
}

#include <math.h>
#include <Trees/math_util.h>

int upper_square(int x)
{
  return (int)pow(2.0, ceil(log2f((float)x)/2.0));
}

int lower_square(int x)
{
  return (int)pow(2.0, floor(log2f((float)x)/2.0));
}

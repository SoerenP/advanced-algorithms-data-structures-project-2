#ifndef _VEBTREE_BASE_24_H_
#define _VEBTREE_BASE_24_H_

#include <Trees/bitstring_utils.h>

#define VEBTREE_BASE_24_OK (0)
#define VEBTREE_BASE_24_ERROR (1)
#define VEBTREE_BASE_24_NIL (-1)
#define VEBTREE_BASE_24_CUTOFF (64)
#define VEBTREE_BASE_24_DEFAULT_SIZE 8388607// 2^23-1, since 24 bit integers


typedef int vebtree_base_24_value;

/* in the base case, min and max er set */
struct vebtree_base_24 {
	int n; 
	int size;
	struct vebtree_base_24 *top;
	struct vebtree_base_24 **bottom; //The ceil(sqrt(u)) vebtree's. 
	struct bitstring bs; //a bitstring for when we reach the cutoff value, where the universe is so small that bitstring suffices
	vebtree_base_24_value min; //does not appear in any of the cluster elements
	vebtree_base_24_value max; //does appear in one of the cluster elements
};

struct vebtree_base_24 *vebtree_base_24_create(int n);
void vebtree_base_24_destroy(struct vebtree_base_24 *tree);
vebtree_base_24_value vebtree_base_24_find_min(struct vebtree_base_24 *tree);
vebtree_base_24_value vebtree_base_24_predecessor_search(struct vebtree_base_24 *tree, vebtree_base_24_value key);
int vebtree_base_24_delete(struct vebtree_base_24 *tree, vebtree_base_24_value key);
int vebtree_base_24_insert(struct vebtree_base_24 *tree, vebtree_base_24_value key);
vebtree_base_24_value vebtree_base_24_find_succ(struct vebtree_base_24 *tree, int key);
vebtree_base_24_value vebtree_base_24_delete_min(struct vebtree_base_24 *tree);

int vebtree_base_24_member(struct vebtree_base_24 *tree, vebtree_base_24_value key);
int vebtree_base_24_high(int n, int x); 
int vebtree_base_24_low(int n, int x); 
int vebtree_base_24_index(int n, int x, int y); 
#define GET_N(t) (t->n)

#endif

#ifndef __rbtree_h_
#define __rbtree_h_

/*
 * Red/Black Tree implementation inspired by Cormen Et. Al 
*/

#define RBTREE_ERROR 1
#define RBTREE_OK 0

enum Color {
	red,
	black
};

struct rbtree {
	struct node *root;
	struct node *nill; /* global nill (NULL) */
};

struct node {
	struct node *left; // 8 byte
	struct node *right; // 8 byte
	struct node *parent; // 8 byte
	int val; // 4 byte
	enum Color color:1;  // 1 bit, but probably padded to 4 byte?
};

/* in case we change our minds to float or something along those lines */
typedef int node_value;

/* constructors */
struct rbtree *rbtree_create(node_value val);
struct node *node_create(struct rbtree *tree, node_value val);

/* destructors (memory cleanup) */
void rbtree_destroy_subtree(struct rbtree *tree, struct node *node);
void rbtree_destroy(struct rbtree *tree);

/* operations on rbtree */
struct node *rbtree_insert(struct rbtree *tree, node_value val);
void rbtree_delete(struct rbtree *tree, struct node *z);
void rbtree_delete_min(struct rbtree *tree);
void rbtree_inorder_walk(struct rbtree *tree, struct node *root);
struct node *rbtree_min(struct rbtree *tree, struct node *root);
struct node *rbtree_max(struct rbtree *tree, struct node *root);
struct node *rbtree_iter_search(struct rbtree *tree, node_value k);

/* defined as min i st. i > x */
struct node *rbtree_successor(struct rbtree *tree, struct node *x);

/* defined as max i st. i < x */
struct node *rbtree_predecessor(struct rbtree *tree, struct node *x);

/* defined as max i st. i <= x */
node_value rbtree_incl_predecessor(struct rbtree *tree, node_value val);

/* correctness of rbtree properties */
int rbtree_check_conditions(struct rbtree *tree, struct node *n);
#endif

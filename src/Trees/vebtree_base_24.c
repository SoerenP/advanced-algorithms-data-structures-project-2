#include <stdlib.h>
#include <math.h>

#include <Trees/dbg.h>
#include <Trees/vebtree_base_24.h>
#include <Trees/math_util.h>
#include <Trees/bool.h>

void vebtree_base_24_destroy(struct vebtree_base_24 *tree)
{
	if(tree->n > VEBTREE_BASE_24_CUTOFF){
		if(tree->top) 
			vebtree_base_24_destroy(tree->top);	
	}
	if(tree->bottom)
	{
		int index;
		for(index = 0; index < upper_square(tree->n); index++)
		{
			if(tree->bottom[index])
				vebtree_base_24_destroy(tree->bottom[index]);
		}
		free(tree->bottom);
	}		
	free(tree);
}

struct vebtree_base_24 *vebtree_base_24_create(int n)
{
  //check(n >= VEBTREE_BASE_24_CUTOFF,
  //"Invalid size parameter: %d",n);
  struct vebtree_base_24 *new = NULL;
  new = malloc(sizeof(*new));
  check_mem(new);

  new->min = VEBTREE_BASE_24_NIL;
  new->max = VEBTREE_BASE_24_NIL;
  new->n = n;
  new->size = 0;
  /* !basecase - need top and bottom */
  if(n > VEBTREE_BASE_24_CUTOFF)
    {
      /* allocate top recursively */
      new->top = NULL;
      new->top = vebtree_base_24_create(upper_square(n));
      
      check_mem(new->top);

      /* allocate sqrt(n) pointers to recursively made structs */
      new->bottom = 
	malloc(sizeof(struct vebtree_base_24 *) * upper_square(n));

      int index;
      for(index = 0; index < upper_square(n); index++)
	{
	  new->bottom[index] = 
	    vebtree_base_24_create(lower_square(n));
	  check_mem(new->bottom[index]);
	}

    } 
  else /* basecase - only 64 elements, store in two integers */
    {
      new->bottom = NULL;
      new->top = NULL;
      new->bs = bitstring_create(VEBTREE_BASE_24_CUTOFF);
    }

  return new;
 error:
  return NULL;
}

vebtree_base_24_value vebtree_base_24_predecessor_search(struct vebtree_base_24 *tree, vebtree_base_24_value key)
{
  int a = vebtree_base_24_high(GET_N(tree),key);
  int b = vebtree_base_24_low(GET_N(tree),key);  
  int j = 0;

  if(tree->size > 2)
  {
    if(GET_N(tree) <= VEBTREE_BASE_24_CUTOFF)
    {
      int b = vebtree_base_24_low(VEBTREE_BASE_24_CUTOFF*VEBTREE_BASE_24_CUTOFF,key);
      j = bitstring_pred(&(tree->bs),b);
      //min is NOT inside rec. structure, so if we find something smaller, we were meant to fint it (i.e 0 < tree->min)
      if(tree->min > j) j = tree->min;
    }
    else if(tree->bottom[a-1]->min <= b)
    {
	//printf("key %d, a %d, b %d, bottom(a-1)->min %d\n",key,a,b,tree->bottom[a-1]->min);
	int lower_pred = vebtree_base_24_predecessor_search(tree->bottom[a-1],b);
	j = vebtree_base_24_index(GET_N(tree), a, lower_pred);
    }
    else
    {
    	int c = vebtree_base_24_predecessor_search(tree->top, a-1);
	j = vebtree_base_24_index(GET_N(tree), c, tree->bottom[c-1]->max);
	if(key <= b) j = tree->min; //this seems to be an edge case? For the very first query on the very first leftmost block TODO dirty?
    }
  }
  else
  {
    if(key == tree->min)
      j = tree->min;
    else
      j = tree->max;
  }
  
  //min is outside the recursive structure, so in the basecase we did not find it, if thats the last part we need. so check here. 
  if(key >= tree->min && tree->min > j)
  {
    j = tree->min;
  }

  return j;
}

vebtree_base_24_value vebtree_base_24_find_succ(struct vebtree_base_24 *tree, int key)
{
  int a = vebtree_base_24_high(GET_N(tree),key);
  int b = vebtree_base_24_low(GET_N(tree),key);
  int j = 0;

  if (tree->size > 2)
    {
      if (GET_N(tree) <= VEBTREE_BASE_24_CUTOFF)
	{
	  int b = vebtree_base_24_low(GET_N(tree)*GET_N(tree),key);
	  j = bitstring_succ(&(tree->bs),b);
	}
      else if (tree->bottom[a-1]->max >= b)
	{
	  j = vebtree_base_24_index(GET_N(tree), a, vebtree_base_24_find_succ(tree->bottom[a-1], b));
	}
      else
	{
	  int c = vebtree_base_24_find_succ(tree->top, a + 1);
	  j = vebtree_base_24_index(GET_N(tree), c, tree->bottom[c-1]->min);
	}
    }
  else
    {
      if (key == tree->min)
	{
	  j = tree->min;
	}
      else
	{
	  j = tree->max;
	}
    }

  if (key <= tree->min && tree->min < j)
    {
      j = tree->min;
    }
  return j;
}

vebtree_base_24_value vebtree_base_24_find_min(struct vebtree_base_24 *tree)
{
  return tree->min;
}

vebtree_base_24_value vebtree_base_24_find_max(struct vebtree_base_24 *tree)
{
  return tree->max;
}

vebtree_base_24_value vebtree_base_24_delete_min(struct vebtree_base_24 *tree)
{
  return vebtree_base_24_delete(tree,vebtree_base_24_find_min(tree));
}

int vebtree_base_24_delete(struct vebtree_base_24 *tree, vebtree_base_24_value key)
{
  check(tree != NULL, "Recieved null pointer");
  check(key <= tree->n, "key must be in [1..%d]",tree->n);

  if(tree->size <= 1)
  {
    tree->min = VEBTREE_BASE_24_NIL;
    tree->max = VEBTREE_BASE_24_NIL;
    tree->size = 0;
    return VEBTREE_BASE_24_OK;
  }
  else if(tree->size >= 2)
  {
    if(GET_N(tree) == VEBTREE_BASE_24_CUTOFF)
    {
      if(key == tree->min)
      {
        tree->min = bitstring_succ(&(tree->bs),tree->min);
        bitstring_delete_bit(&(tree->bs),tree->min);
      }
      else
      {
        bitstring_delete_bit(&(tree->bs),key);
        if(key == tree->max)
        {
	  tree->max = bitstring_pred(&(tree->bs),tree->max);
	}
      }
    }
    else
    {
      if(key == tree->min){
        int a = vebtree_base_24_find_min(tree->top);
        int b = vebtree_base_24_find_min(tree->bottom[a-1]);
        tree->min = vebtree_base_24_index(GET_N(tree),a,b);
        key = tree->min;
      }

      int a = vebtree_base_24_high(GET_N(tree),key);
      int b = vebtree_base_24_low(GET_N(tree),key);
      vebtree_base_24_delete(tree->bottom[a-1],b);

      if(tree->bottom[a-1]->size == 0)
      {
        vebtree_base_24_delete(tree->top,a);
      }

      if(key == tree->max && tree->top->size > 0)
      {
        int a = vebtree_base_24_find_max(tree->top);
        int b = vebtree_base_24_find_max(tree->bottom[a-1]);
        tree->max = vebtree_base_24_index(GET_N(tree),a,b);
      }
    }
  }

  tree->size--;
  return VEBTREE_BASE_24_OK;
error:
  return VEBTREE_BASE_24_ERROR;
}	

static int vebtree_base_24_insert_empty(struct vebtree_base_24 *tree, vebtree_base_24_value key)
{
  
  tree->min = key;
  tree->max = key;
  tree->size = 1;
  return VEBTREE_BASE_24_OK;
}

int vebtree_base_24_insert(struct vebtree_base_24 *tree, vebtree_base_24_value key)
{
  check(key <= GET_N(tree), "Key must be in [1..%d], got %d, tree->size %d",tree->n, key, tree->size);
  /* the tree is empty */
  if (tree->size == 0){
    vebtree_base_24_insert_empty(tree,key);
    return VEBTREE_BASE_24_OK;
  }
  else if(tree->size > 0) /* the tree is not empty */
  {
    if(key < tree->min){
      /* new min? exchange */
      vebtree_base_24_value temp = key;
      key = tree->min;
      tree->min = temp;
    }

    if(tree->size >= 1 && GET_N(tree) > VEBTREE_BASE_24_CUTOFF){

      int a = vebtree_base_24_high(GET_N(tree),key);
      int b = vebtree_base_24_low(GET_N(tree),key);
      if(tree->bottom[a-1]->size == 0)
        {
	  vebtree_base_24_insert(tree->top,a);
        }
  
      vebtree_base_24_insert(tree->bottom[a-1],b);

    } 
    else if(GET_N(tree) <= VEBTREE_BASE_24_CUTOFF){
      bitstring_insert_bit(&(tree->bs),key);
    }

    tree->size++;

    if(key > tree->max)
      {
        tree->max = key;
      }
  }	
  return VEBTREE_BASE_24_OK;
error:
  return VEBTREE_BASE_24_ERROR;
}

int vebtree_base_24_member(struct vebtree_base_24 *tree, vebtree_base_24_value key)
{
  if(key == tree->max || key == tree->min)
    { return TRUE; }
  else if(GET_N(tree) <= VEBTREE_BASE_24_CUTOFF) //basecase
    { return bitstring_test_bit(&tree->bs, key); }
  else
    {
      int a = vebtree_base_24_high(GET_N(tree), key);
      int b = vebtree_base_24_low(GET_N(tree),key);
      return vebtree_base_24_member(tree->bottom[a-1],b);
    }
}

/* universe size is the size of the overall root	*/
/* since the entire structure covers {0...u-1} elements */
int vebtree_base_24_high(int n, int x)
{
  x--;
  return ((int)floor(x/lower_square(n))) +1;
}

int vebtree_base_24_low(int n, int x)
{
  x--;
  return (x % lower_square(n))+1;
}

int vebtree_base_24_index(int n, int x, int y)
{
  return (x-1) * lower_square(n) + y;
}

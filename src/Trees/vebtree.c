#include <stdlib.h>
#include <math.h>

#include <Trees/dbg.h>
#include <Trees/vebtree.h>
#include <Trees/math_util.h>

void vebtree_destroy(struct vebtree *tree)
{
	if(tree->top) vebtree_destroy(tree->top);	
	if(tree->bottom)
	{
		int index;
		for(index = 0; index < upper_square(tree->n); index++)
		{
			if(tree->bottom[index])
				vebtree_destroy(tree->bottom[index]);
		}
		free(tree->bottom);
	}		
	free(tree);
}

struct vebtree *vebtree_create(int n)
{
  check(n >= VEBTREE_BASE, "Invalid size parameter: %d",n);
  struct vebtree *new = NULL;

  new = malloc(sizeof(*new));
  check_mem(new);

  new->min = VEBTREE_NIL;
  new->max = VEBTREE_NIL;
  new->n = n;
  new->size = 0;
  /* !basecase - need top and bottom */
  if(n > VEBTREE_BASE)
    {
      /* allocate top recursively */
      new->top = NULL;
      new->top = vebtree_create(upper_square(n));
      check_mem(new->top);

      /* allocate sqrt(n) pointers to recursively made structs */
      new->bottom = 
	malloc(sizeof(struct vebtree *) * upper_square(n));

      int index;
      for(index = 0; index < upper_square(n); index++)
	{
	  new->bottom[index] = 
	    vebtree_create(lower_square(n));
	  check_mem(new->bottom[index]);
	}

    } 
  else /* basecase - only 2 elements, only set min/max */
    {
      new->top = NULL;
      new->bottom = NULL;
    }

  return new;
 error:
  return NULL;
}

vebtree_value vebtree_find_succ(struct vebtree *tree, int key)
{
  int a = vebtree_high(GET_N(tree),key);
  int b = vebtree_low(GET_N(tree),key);
  int j = 0;

  if (tree->size > VEBTREE_BASE)
    {
      if (tree->bottom[a]->max >= b)
	{
	  j = vebtree_index(GET_N(tree), a, vebtree_find_succ(tree->bottom[a], b));
	}
      else
	{
	  int c = vebtree_find_succ(tree->top, a + 1);
	  
	  j = vebtree_index(GET_N(tree), c, tree->bottom[c]->min);
	  //printf("c is %d and bottom[c].min is %d and j is %d\n", c, tree->bottom[c]->min, j);
	}
    }
  else
    {
      if (key == tree->min)
	{
	  j = tree->min;
	}
      else
	{
	  j = tree->max;
	}
    }

  if (key <= tree->min && tree->min < j)
    {
      j = tree->min;
    }
  //printf("Looked for successor to %d in tree of size %d and returned %d with a=%d and b=%d\n", key, tree->size, j, a, b);
  return j;
}

vebtree_value vebtree_find_min(struct vebtree *tree)
{
  return tree->min;
}

vebtree_value vebtree_find_max(struct vebtree *tree)
{
  return tree->max;
}

vebtree_value vebtree_predecessor_search(struct vebtree *tree, vebtree_value key)
{
  int a = vebtree_high(GET_N(tree),key);
  int b = vebtree_low(GET_N(tree),key);  
  int j = 0;
  if(tree->size > VEBTREE_BASE)
  {
    if(tree->bottom[a]->min <= b)
      {
	int lower_pred = vebtree_predecessor_search(tree->bottom[a],b);
	j = vebtree_index(GET_N(tree), a, lower_pred);

      }
    else
      {
        
	int c = 0;
	
	if (tree->top->min > a)
	  j = tree->min;
	else if (tree->top->min == a)
	  {
	    if (tree->bottom[a]->min <= b)
	      {
		j = vebtree_predecessor_search(tree->bottom[a], b);
	      }
	    else
	      {
		j = tree->min;
	      }
	  }
	else
	  {
	    c = vebtree_predecessor_search(tree->top, a - 1);
	    j = vebtree_index(GET_N(tree), c, tree->bottom[c]->max);
	  }
	
      }
  }
  else
  {
    if(key >= tree->max)
      j = tree->max;
    else
      j = tree->min;
  }
  
  //min is outside the recursive structure, so in the basecase we did not find it, if thats the last part we need. so check here. 
  /*if(key >= tree->min && key < j)
  {
    j = tree->min;
    }*/

  return j;
}

int vebtree_delete_min(struct vebtree *tree)
{
	return vebtree_delete(tree,vebtree_find_min(tree));
}

int vebtree_delete(struct vebtree *tree, vebtree_value key)
{
  check(tree != NULL, "Recieved null pointer");
  check(key <= tree->n, "key must be in [1..%d]",tree->n);

  if(tree->size <= 1)
  {
    tree->min = VEBTREE_NIL;
    tree->max = VEBTREE_NIL;
    tree->size = 0;
    return VEBTREE_OK;
  } 
  else if(tree->size == VEBTREE_BASE)
  {
    if(tree->min == key)
    {
      tree->min = tree->max; //could also be 1
    }
    tree->max = tree->min;
    tree->size = 1;
  }
  else if(tree->size > VEBTREE_BASE) 
  {
    if(tree->min == key)
    {
      int a = vebtree_find_min(tree->top);
      int b = vebtree_find_min(tree->bottom[a]);
      tree->min = vebtree_index(tree->n, a, b);
      key = tree->min;
    }

    /* delete from version 1 */
    int a = vebtree_high(tree->n,key);
    int b = vebtree_low(tree->n,key);

    vebtree_delete(tree->bottom[a],b);

    if(tree->bottom[a]->size == 0)
    {
      vebtree_delete(tree->top, a);
    }

    tree->size--;

    /* if we deleted max, find new max */
    if(key == tree->max){
      int a = vebtree_find_max(tree->top);
      int b = vebtree_find_max(tree->bottom[a]);
      tree->max = vebtree_index(tree->n, a, b);
      
      //if (tree->max == VEBTREE_NIL) printf("(B) Set max to -1 with key %d, tree size %d and n = %d, a = %d and b = %d\nThe bottom tree from which b came has size %d, min value %d and max %d\n", key, tree->size, GET_N(tree), a, b, tree->bottom[a]->size, tree->bottom[a]->min, tree->bottom[a]->max);
    }
  }

  return VEBTREE_OK;
error:
  return VEBTREE_ERROR;
}	

static int vebtree_insert_empty(struct vebtree *tree, vebtree_value key)
{
  tree->min = key;
  tree->max = key;
  tree->size = 1;
  return VEBTREE_OK;
}

int vebtree_insert(struct vebtree *tree, vebtree_value key)
{
  check(key <= GET_N(tree), "Key must be in [1..%d], got %d",GET_N(tree), key);
  //printf("Tried to insert %d into a tree of N value %d\n", key, GET_N(tree));
  /* the tree is empty */
  if (tree->size == 0){
    vebtree_insert_empty(tree,key);
    //printf("Inserted %d into an empty tree\n", key);
    return VEBTREE_OK;
  }
  else if(tree->size > 0) /* the tree is not empty */
  {
    if(key < tree->min){
      /* new min? exchange */
      vebtree_value temp = key;
      key = tree->min;
      tree->min = temp;
      //printf("Set new min to %d\n", key);
    }

    if(tree->size >= 1 && GET_N(tree) > VEBTREE_BASE){

      int a = vebtree_high(GET_N(tree),key);
      int b = vebtree_low(GET_N(tree),key);

      //printf("Got a=%d and b=%d for n=%d and key=%d\n", a,b,GET_N(tree),key);
      
      /* if the top is empty, we need to insert something into it */
      if(tree->bottom[a]->size == 0)
        {
          vebtree_insert(tree->top,a);
        }
  
      vebtree_insert(tree->bottom[a],b);

    }

    tree->size++;

    if(key > tree->max)
      {
        tree->max = key;
	//printf("Set new max to %d\n", key);
      }
    }	
  return VEBTREE_OK;
error:
  return VEBTREE_ERROR;
}

int vebtree_member(struct vebtree *tree, vebtree_value key)
{
  if(key == tree->max || key == tree->min)
    { return 1; }
  else if(tree->size <= 2) //basecase
    { return 0; }
  else return vebtree_member(tree->bottom[vebtree_high(GET_N(tree),key)],vebtree_low(GET_N(tree),key));
}

/* universe size is the size of the overall root	*/
/* since the entire structure covers {0...u-1} elements */
int vebtree_high(int n, int x)
{
  return (int)floor(x/lower_square(n));
}

int vebtree_low(int n, int x)
{
  return x % lower_square(n);
}

int vebtree_index(int n, int x, int y)
{
  return x * lower_square(n) + y;
}


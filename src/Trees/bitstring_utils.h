#ifndef _BITSTRING_UTILS_H_
#define _BITSTRING_UTILS_H_

#define BIT_ARRAY_SIZE 2

struct bitstring {
  int bits[BIT_ARRAY_SIZE];
  int n;
};

struct bitstring bitstring_create(int bits);

void bitstring_insert_bit(struct bitstring *bs, int x);

void bitstring_delete_bit(struct bitstring *bs, int x);

int bitstring_test_bit(struct bitstring *bs, int x);

int bitstring_succ(struct bitstring *bs, int x);

int bitstring_pred(struct bitstring *bs, int x);

int bitstring_msb_naive(struct bitstring *bs);

int bitstring_lsb_naive(struct bitstring *bs); 


#endif

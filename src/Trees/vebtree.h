#ifndef _VEBTREE_H_
#define _VEBTREE_H_

#define VEBTREE_OK (0)
#define VEBTREE_ERROR (1)
#define VEBTREE_NIL (-1)
#define VEBTREE_BASE (2)

typedef int vebtree_value;

/* in the base case, min and max er set */
struct vebtree {
	int n; 
	int size;
	struct vebtree *top; 
	struct vebtree **bottom; //The ceil(sqrt(u)) vebtree's. 
	vebtree_value min; //does not appear in any of the cluster elements
	vebtree_value max; //does appear in one of the cluster elements
};

struct vebtree *vebtree_create(int n);
void vebtree_destroy(struct vebtree *tree);
vebtree_value vebtree_find_min(struct vebtree *tree);
vebtree_value vebtree_predecessor_search(struct vebtree *tree, vebtree_value key);
int vebtree_delete(struct vebtree *tree, vebtree_value key);
int vebtree_insert(struct vebtree *tree, vebtree_value key);
int vebtree_delete_min(struct vebtree *tree);
vebtree_value vebtree_find_succ(struct vebtree *tree, int key);
int vebtree_member(struct vebtree *tree, vebtree_value key);
int vebtree_high(int n, int x); 
int vebtree_low(int n, int x); 
int vebtree_index(int n, int x, int y); 

#define GET_N(t) (t->n)

#endif

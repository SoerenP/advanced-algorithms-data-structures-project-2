#include <stdlib.h>
#include <stdio.h>
#include <Trees/rbtree.h>
#include <Trees/dbg.h>

struct rbtree *rbtree_create(node_value val)
{
	struct rbtree *new = malloc(sizeof(*new));
	check_mem(new);	

	// skal ikke være nul, skal bare være et blad der er sort?
	new->nill = NULL;
	struct node *sentinel = node_create(new,-1);
	sentinel->color = black;
	sentinel->parent = sentinel;
	sentinel->left = sentinel;
	sentinel->right = sentinel;
	new->nill = sentinel;

	struct node *root = node_create(new,val);
	check_mem(root);
	root->color = black;
	root->parent = sentinel;
	new->root = root;
	return new;
error:
	if(new) free(new);
	if(root) free(root);
	return NULL;
}

void rbtree_destroy_subtree(struct rbtree *tree, struct node *node)
{
	if(node != NULL && node != tree->nill){	
		if(node->left != NULL && node != tree->nill)
			rbtree_destroy_subtree(tree,node->left);
		if(node->right != NULL && node->right != tree->nill)
			rbtree_destroy_subtree(tree,node->right);
		free(node);
	}
}

void rbtree_destroy(struct rbtree *tree)
{
	if(tree != NULL){
		struct node *root = tree->root;
		if(root->left != NULL && root->left != tree->nill)
			rbtree_destroy_subtree(tree,root->left);
		if(root->right != NULL && root->right != tree->nill)
			rbtree_destroy_subtree(tree,root->right);
		if(root != NULL && root != tree->nill)
			free(root);		
		if(tree->nill) free(tree->nill);
		free(tree);	
	}
}

struct node *node_create(struct rbtree *tree, node_value val){
	struct node *new = malloc(sizeof(*new));
	if(new == NULL) return NULL;	
	new->color = red;
	new->parent = tree->nill;
	new->left = tree->nill;
	new->right = tree->nill;
	new->val = val;
	return new;
}

static void rbtree_rotate_left(struct rbtree *tree, struct node *x)
{
	if(x->right == tree->nill) printf("left rotate, x->right is NILL\n");

	struct node *y;
	y = x->right;
	x->right = y->left;
	if(y->left != tree->nill) y->left->parent = x;
	y->parent = x->parent;
	if(x->parent == tree->nill) tree->root = y;
	else if(x == x->parent->left) x->parent->left = y;
	else x->parent->right = y;
	y->left = x;
	x->parent = y;
}

static void rbtree_rotate_right(struct rbtree *tree, struct node *x)
{
	struct node *y;
	y = x->left;
	x->left = y->right;
	if(y->right != tree->nill){y->right->parent = x;}
	y->parent = x->parent;
	if(x->parent == tree->nill) tree->root = y;
	else if(x == x->parent->right) x->parent->right = y;
	else x->parent->left = y;
	y->right = x;
	x->parent = y;
}

static void rbtree_transplant(struct rbtree *tree, struct node *u, struct node *v)
{
	if(u->parent == tree->nill) tree->root = v;
	else if(u == u->parent->left) u->parent->left = v;
	else u->parent->right = v;
	v->parent = u->parent;
}

static void rbtree_insert_fixup(struct rbtree *tree, struct node *z)
{
	struct node *y;

	while(z->parent->color == red)
	{
		if(z->parent == z->parent->parent->left)
		{
			y = z->parent->parent->right;			
			if((y != NULL) && y->color == red) //ups
			{
				z->parent->color = black;
				y->color = black;
				z->parent->parent->color = red;
				z = z->parent->parent;
			} else {
				if(z == z->parent->right){
					z = z->parent;			
					rbtree_rotate_left(tree, z);
				}
				z->parent->color = black;
				z->parent->parent->color = red;
				rbtree_rotate_right(tree,z->parent->parent);			
			} 
		} else {
			y = z->parent->parent->left;
			if(y->color == red)
			{
				z->parent->color = black;
				y->color = black;
				z->parent->parent->color = red;
				z = z->parent->parent;
			} else {
				if(z == z->parent->left){
					z = z->parent;
					rbtree_rotate_right(tree,z);
				}
				z->parent->color = black;
				z->parent->parent->color = red;
				rbtree_rotate_left(tree,z->parent->parent);
			}
		}
	}
	tree->root->color = black;
	tree->root->parent = tree->nill; //hm?
}

struct node *rbtree_insert(struct rbtree *tree, node_value val)
{
	struct node *y,*x;
	struct node *z = node_create(tree,val);

	y = tree->nill;
	x = tree->root;
	while(x != tree->nill)
	{
		y = x;
		if(z->val < x->val) x = x->left;
		else x = x->right;	
	}
	z->parent = y;
	if(y == tree->nill) tree->root = z;
	else if(z->val < y->val) y->left = z;
	else y->right = z;

	z->left = tree->nill;
	z->right = tree->nill;
	z->color = red;
	rbtree_insert_fixup(tree,z);
	return z;
}

static void rbtree_delete_fixup(struct rbtree *tree, struct node *x)
{
	struct node *w;
	while((x != tree->root) && (x->color == black))
	{
		if(x == x->parent->left){
			w = x->parent->right;
			if(w->color == red){
				w->color = black;
				x->parent->color = red;
				rbtree_rotate_left(tree,x->parent);
				w = x->parent->right;
			}
			if(w->left->color == black && w->right->color == black){
				w->color = red;
				x = x->parent;
			} else { 
				if(w->right->color == black){
					w->left->color = black;
					w->color = red;
					rbtree_rotate_right(tree,w);
					w = x->parent->right;
				}
				w->color = x->parent->color;
				x->parent->color = black;
				w->right->color = black;
				rbtree_rotate_left(tree,x->parent);
				x = tree->root;
			}
		} else {
			w = x->parent->left;
			if(w->color == red){
				w->color = black;
				x->parent->color = red;
				rbtree_rotate_right(tree,x->parent);
				w = x->parent->left;
			}
			
			if(w->right->color == black && w->left->color == black)
			{
				w->color = red;
				x = x->parent;
			} else {
				if(w->left->color == black){
					w->right->color = black;
					w->color = red;
					rbtree_rotate_left(tree,w);
					w = x->parent->left;
				}
				w->color = x->parent->color;
				x->parent->color = black;
				w->left->color = black;
				rbtree_rotate_right(tree,x->parent);
				x = tree->root;
			}
		}
	}
	x->color = black;
}


void rbtree_delete(struct rbtree *tree, struct node *z)
{
	struct node *y,*x;
	y = z;
	
	enum Color y_orig_color = y->color;
	if(z->left == tree->nill){
		x = z->right; //crashes here
		rbtree_transplant(tree,z,z->right);
	} else if(z->right == tree->nill) {
		x = z->left;
		rbtree_transplant(tree,z,z->left);
	} else {
		y = rbtree_min(tree,z->right);
		y_orig_color = y->color;
		x = y->right;
		if(y->parent == z) x->parent = y;
		else {
			rbtree_transplant(tree,y,y->right);
			y->right = z->right;
			y->right->parent = y;
		}
		rbtree_transplant(tree,z,y);
		y->left = z->left;
		y->left->parent = y;
		y->color = z->color;
	}
	if(y_orig_color == black) rbtree_delete_fixup(tree,x);
}


struct node *rbtree_iter_search(struct rbtree *tree, node_value k)
{
	struct node *x = tree->root;
	while((x != NULL) && (k != x->val) && (x != tree->nill))
	{
		if(k < x->val) x = x->left;
		else x = x->right;
	}
	if(x == tree->nill) x = x->parent;
	return x;
}

node_value rbtree_pred(struct rbtree *tree, struct node *root, node_value val, node_value sofar)
{
	// vi fandt target, stop
	if(root->val == val) return root->val;

	//Hvis noden vi er på er mindre, skal vi til højre
	//hvis vi ikke kan komme til højre, er vi ved pred
	if(root->val < val && (root->right != tree->nill))
		return rbtree_pred(tree,root->right,val,root->val);
	else if(root->val < val && (root->right == tree->nill))
		return root->val;

	//Hvis noden vi er på er større, skal vi til venstre
	//Hvis vi ikke kan komme til venstre, er der intet element mindre, men elementet selv er der heller ikke
	if(root->val > val && (root->left != tree->nill))
		return rbtree_pred(tree,root->left,val,sofar);
	else if(root->val > val && (root->left == tree->nill))
		return sofar;
	return 0;
}

node_value rbtree_incl_predecessor(struct rbtree *tree, node_value val)
{
	return rbtree_pred(tree,tree->root, val, val);
}

struct node *rbtree_successor(struct rbtree *tree, struct node *x)
{
	struct node *y;
	y = rbtree_iter_search(tree,x->val);
	if(y != NULL || y != tree->nill){
		return y;
	}
	else 
	{

		if((x->right != NULL) && (x->right != tree->nill)){
			return rbtree_min(tree,x->right);
		}
		y = x->parent;
		while((y != NULL) && (x == y->right) && (y != tree->nill))
		{
			x = y;
			y = y->parent;
		}
		return y;
	}
}

struct node *rbtree_predecessor(struct rbtree *tree, struct node *x)
{
	if((x->left != NULL) && (x->left != tree->nill))
	{
		return rbtree_max(tree,x->left);
	}
	struct node *y;
	y = x->parent;
	while((y != NULL && x == y->left) && (y != tree->nill))
	{
		x = y;
		y = y->parent;
	}
	return y;
}

struct node *rbtree_max(struct rbtree *tree, struct node *root)
{
	while ((root->right != NULL) && (root->right != tree->nill))
	{
		root = root->right;
	}
	return root;
}

struct node *rbtree_min(struct rbtree *tree, struct node *root)
{
	while ((root->left != NULL) && (root->left != tree->nill))
	{
		root = root->left;
	}
	return root;
}

void rbtree_delete_min(struct rbtree *tree)
{
	struct node *temp = rbtree_min(tree,tree->root);
	rbtree_delete(tree,temp);
	free(temp);
}

void rbtree_inorder_walk(struct rbtree *tree, struct node *root)
{
	if(root != NULL && root != tree->nill){
		rbtree_inorder_walk(tree,root->left);
		rbtree_inorder_walk(tree,root->right);	
	}
}

int rbtree_check_conditions(struct rbtree *tree, struct node *n)
{
	if(n == tree->nill && n->color != black){
		printf("\terror, leaf not black %p\n",n);
		return 1;
	}

	if(n == tree->root && n->parent != tree->nill){
		printf("\terror, parent of root not nill %p\n",n);
		return 1;
	}
	
	if(n == tree->root && n->color != black){
		printf("\terror, root not black %p\n",n);
		return 1;
	}

	if((n->color == red) && !((n->left->color == black) && (n->right->color == black))){
		printf("\terror, red children %p\n",n);
		return 1;
	}

	int left = 0; int right = 0;
	if(n->left != tree->nill){
		left = rbtree_check_conditions(tree,n->left);
	}

	if(n->right != tree->nill){
		 right = rbtree_check_conditions(tree,n->right);
	}

	return left + right;
}
